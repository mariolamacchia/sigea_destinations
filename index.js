var argv = require('optimist').argv,
    fs = require('fs'),
    async = require('async'),
    mysql = require('mysql'),
    destinations = require('./destinations');

var connection = mysql.createConnection({
    host: argv.db.host || 'localhost',
    port: argv.db.port || 3306,
    user: argv.db.user || 'root',
    password: argv.db.password || '',
    database: argv.db.name
});

var throwError = function(err) {
    console.log(err);
    process.exit();
}

var getStart = function(connection, cb) {
    var q = 'select indirizzo, cap, localita, provin from ' +
        'fviaggio inner join bancadati on bancadati.codice=carico where ' +
        'fviaggio.progr = ' + argv.fviaggio.toString();
    connection.query(q, function(err, rows) {
        if (err) return throwError(err);
        var r = rows[0];
        var start = r.indirizzo + ' ' + r.cap + ' ' + r.localita + ' ' +
            r.provin;
        cb(connection, start);
    });
}

var listLvet = function(connection, start) {
    var q = 'select letvet.progr, indirizzo, cap, localita, provin from ' +
        'letvet inner join bancadati on bancadati.codice=destinatario where ' +
        'fvprogr = ' + argv.fviaggio.toString();
    connection.query(q, function(err, rows) {
        if (err) return throwError(err);
        for (var i in rows) {
            var r = rows[i];
            var address = r.indirizzo + ' ' + r.cap + ' ' + r.localita + ' ' +
                r.provin;
            destinations.add({
                address: address,
                number: r.progr
            });
        }
        destinations.calc(start, function(e, data) {
            var html = '';

            // Put results into db
            if (e) return throwError(e);
            async.series([
                function(done) {
                    async.concat(data, function(item, next) {
                        var q = 'update letvet set ordine=' + 
                            item.order.toString() + 
                            ', indirizzo_google="' + item.address + '" ' +
                            'where fvprogr=' + item.number;
                        connection.query(q, next);

                        // Save html instructions into html
                        html += item.html;
                    }, function(err) {
                        done(err);
                    });
                },
                function(done) {
                    fs.writeFile(agrv.fviaggio.toString() + '.html', html, done);
                }
            ], function(err) {
                var code = err ? 1 : 0;
                process.exit(code);
            });
        });
    });
}

connection.connect(function(err) {
    if (err) throwError(err);
    getStart(connection, listLvet);
});
