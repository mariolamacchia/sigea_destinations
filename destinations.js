var https = require('https'),
    url = require('url');

var destinations = [];

var handleResponse = function(res) {
    var data = JSON.parse(res);
    console.log(data);
    var ret = [];
    var order = data.routes[0].waypoint_order;
    for (var i in order) {
        // Calculate number, order and address
        var value = {
            number: destinations[order[i]].number,
            order: i,
            address: data.routes[0].legs[i].end_address,
            html: ''
        };
        var steps = data.routes[0].legs[i].steps;

        // Add html instructions
        for (var i in steps) {
            var step = steps[i];
            value.html += '<div>' + step.html_instructions + '</div>';
        }
        ret.push(value);
    }
    return ret;
}

var getHost = function(sp) {
    host = 'https://maps.googleapis.com/maps/api/directions/json?';
    host += 'origin=' + sp + '&destination=' + sp;
    host += '&waypoints=optimize:true|';
    for (var i in destinations) {
        host += destinations[i].address += '|';
    }
    console.log(host);
    return url.parse(host);
}

exports.add = function(dest) {
    destinations.push(dest);
}

exports.calc = function(sp, cb) {
    https.get(getHost(sp), function(res) {
        var str = '';
        res.on('error', cb);
        res.on('data', function(data) {
            str += data.toString('utf-8');
        });
        res.on('end', function() {
            cb(null, handleResponse(str), JSON.parse(str));
        });
    });
}
